#!/bin/bash

install_wp_cli_yml () {
        echo "checking if file named wp-cli.yml exists"
        if [ ! -f ./wp-cli.yml ]; then
                echo "Installing wp-cli.yml"
                echo "apache_modules:" > wp-cli.yml
                echo -e "\t- mod_rewrite" >> wp-cli.yml
                echo "File wp-cli.yml is installed!"
        else
                echo "It exists!"
        fi
}
staging_install()
{
        echo "installing...."
        echo "current database is doing backup...."
        backup=$(date +%Y-%m-%d-%H-%M)_backup.sql
        wp db export $backup
        mkdir ../backup
        mv ./$backup ../backup
        wp migratedb export staging.sql --find=$new_current_domain --replace=$staging_domain
        mkdir ../staging
        mv ./staging.sql ../staging
        echo "copying all files to staging"
        #if cp -r ./* ../staging; then
        du -sb 
        if rsync -r ./* ../staging --info=progress2; then
                cp_status=$?
                #rsync -r ./* ../staging --progress
                if [[ "$cp_status" == "0" ]]; then
                        echo "Staging is copied!"
                        cd ../staging
                        echo "generating wp config"
                        wp config create --dbname=$new_dbname --dbuser=$new_dbuser --dbpass=$new_dbpass --dbhost=$new_dbhost --dbprefix=$dbprefix --force --extra-php="define( 'WP_DEBUG', true );define( 'WP_DEBUG_LOG', true );define( 'WP_DEBUG_DISPLAY', false );"
                        cat wp-config.php | grep DB
                        wp db import staging.sql
                        #wp rewrite structure '/%postname%/'
                        #Flush wp rewrite to activate htaccess
                        pwd=$(pwd)
                        htpasswd -b -c ./.htpasswd edream edream
                        echo -e "AuthType Basic\nAuthName \"Site is protected with password. User:edream Pass:edream\"\nAuthUserFile $pwd/.htpasswd\nRequire valid-user" > .htaccess
                        install_wp_cli_yml
                        wp rewrite flush --hard
                        #Discourage search engines
                        wp option set blog_public 0
                        echo "staging environment is ready!"
                        echo $staging_domain
                fi
        fi
        exit
}
insert_db_access()
{
        #cd htdocs
        wp core version
        echo "checking if plugin named WP Migrate DB exists"
        #check=$(wp plugin get wp-migrate-db --field=name)
        wp plugin is-installed wp-migrate-db
        check=$?
        if [ "$check" == "0" ]; then
                echo "the plugin exists"
                #wp plugin activate wp-migrate-db
                #exit 0
                dbname=$(wp config get --constant=DB_NAME)
                dbuser=$(wp config get --constant=DB_USER)
                dbpass=$(wp config get --constant=DB_PASSWORD)
                dbhost=$(wp config get --constant=DB_HOST)
                dbprefix=$(cat wp-config.php | grep "\$table_prefix" | cut -d \' -f 2)
                #current_domain=$(wp option get home)

                echo "Insert database name [$dbname]?"
                read -e new_dbname
                if [[ -z "$new_dbname" ]]; then
                        new_dbname=$dbname;
                fi
                echo "Insert database user [$dbuser]?"
                read -e new_dbuser
                if [[ -z "$new_dbuser" ]]; then
                        new_dbuser=$dbuser;
                fi
                echo "Insert database pass [$dbpass]?"
                read -e new_dbpass
                if [[ -z "$new_dbpass" ]]; then
                        new_dbpass=$dbpass;
                fi
                echo "Insert database host [$dbhost]?"
                read -e new_dbhost
                if [[ -z "$new_dbhost" ]]; then
                        new_dbhost=$dbhost;
                fi
                echo "Insert current domain host?"
                read -e new_current_domain
                while [[ -z "$new_current_domain" ]]; do
                        echo "Insert current domain host?"
                        read -e new_current_domain
                done
                echo "Insert staging domain?"
                read -e staging_domain
                while [ -z "$staging_domain" ]; do
                        echo "Insert staging domain?"
                        read -e staging_domain
                done
                echo "Inserted staging access:"
                echo "------------------------"
                echo "database: $new_dbname"
                echo "user: $new_dbuser"
                echo "pass: $new_dbpass"
                echo "host: $new_dbhost"
                echo "current_domain: $new_current_domain"
                echo "staging_domain: $staging_domain"
                echo "------------------------"
                read -p "are these correct? [Y/n]" db_check
                if [[ "$db_check" == "Y" ]]; then
                        echo "YES"
                        staging_install
                else
                        echo "NO"
                        echo "type wp-staging.sh again!"
                        exit 0
                fi
        else
                echo "please type here: wp plugin install wp-migrate-db --activate"
                exit 0
        fi
}

current_htdocs=$(basename "$PWD")
if [ "$current_htdocs" == "htdocs" ]; then
        if [ ! -f $HOME/bin/wp ]; then
                echo "WP CLI does not exist"
                echo "Installing WP CLI"
                curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && chmod +x wp-cli.phar && mkdir -p $HOME/bin && mv wp-cli.phar $HOME/bin/wp
                echo "WP CLI is installed!"
                insert_db_access
        else
                insert_db_access
        fi
else
        echo "ERROR! File wp-staging.sh must be in htdocs folder!"
        exit 0
fi